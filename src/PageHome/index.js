import React, { useState, useEffect } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Container, Form, Input } from "reactstrap";

import { API_BASE_URL } from "../constants";

import Comments from "./comments";
import MyTimeline from "../components/MyTimeline";

function PageHome({ history }) {
    const [listUsers, setListUsers] = useState([]);
    const [listPosts, setListPosts] = useState([]);
    const [isOpenComment, setOpenModal] = useState(false);
    const [postId, setPostId] = useState(1);
    const [postItem, setPostItem] = useState(null);

    //get user lists
    useEffect(() => {
        axios.get(`${API_BASE_URL}/users`)
        .then((res) => {
            setListUsers(res.data)
        })
        .catch((e) => console.log("ERRORNYA APA", e))
    }, [])

    useEffect(() => {
        axios.get(`${API_BASE_URL}/posts`)
        .then((res) => setListPosts(res.data))
    }, [])

    function onToggleModal(modalPostId) {
        const modalPostItem = listPosts.find((item) => item.id === modalPostId);

        setPostItem(modalPostItem)
        setOpenModal(!isOpenComment)
        setPostId(modalPostId)
    }

    //go to user's profile page
    function onClickUser(dataUser) {
        history.push({
            pathname: `/${dataUser.username}`,
            state: {
                userId: dataUser.id
            }
        })
    }

    function onSubmitPost(postText) {
        axios.post(`${API_BASE_URL}/posts`, {
            title: 'Test title',
            body: postText,
            userId: 1,
            email: "nabila.lilipaly@gmail.com"
        })
        .then((res)=> {
            setListPosts([res.data, ...listPosts])
        })
    }

    return(
        <Container className="homepage-container">
            { 
                isOpenComment && <Comments postId={postId} isOpen={isOpenComment} toggle={() => onToggleModal(postId)} postItem={postItem} /> 
            }
            <div className="mycircle-wrapper">
                <p>My Circle ({listUsers?.length})</p>
                {
                    listUsers.map((users, index) => (
                        <div key={index} className="user-list-text" onClick={() => onClickUser(users)}>
                            <div className="circle-avatar"/>
                            <div className="circle-user-name">
                                <span>{users?.name}</span>
                                <span>{users?.company?.name}</span>
                            </div>
                        </div>
                    ))
                }
            </div>
            <MyTimeline
                listPosts={listPosts}
                listUsers={listUsers}
                onToggleModal={onToggleModal}
                onSubmitPost={onSubmitPost}
                isHomepage
            />
        </Container>
    )
}

export default withRouter(PageHome);