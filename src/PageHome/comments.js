import React, { useState, useEffect } from "react";
import axios from "axios";
import { Modal, Spinner, Form, FormGroup, Input, Button } from "reactstrap";

import { API_BASE_URL } from "../constants";

export default function Comments({ postId, isOpen, toggle, postItem }) {
    const [isFetching, setFetching] = useState(false);
    const [commentList, setCommentList] = useState([]);
    const [commentText, setCommentText] = useState("");
    const [isEditingComment, setEditingComment] = useState(false);
    const [editCommentText, setEditCommentText] = useState("");

    useEffect(() => {
        setFetching(true)

        axios.get(`${API_BASE_URL}/comments?postId=${postId}`)
        .then((res) => {
            setFetching(false)
            setCommentList(res.data)
        })
        .catch((e) => setFetching(false))
    }, [])

    function onSubmitComment() {
        axios.post(`${API_BASE_URL}/comments`, {
            userId: 1,
            postId: postId,
            name: "TEST COMMENT",
            body: commentText,
            email: "nabila.lilipaly@gmail.com"
        })
        .then((res) => {
            const newListComment = [...commentList, res.data];

            //mock new comment list with fake data (submitted before)
            setCommentList(newListComment);
            setCommentText("");
        })
        .catch((e) => {
            console.log("ERROR SUBMIT COMMENT", e)
        })
    }

    function onDeleteComment(commentBody) {
        //I use post method since delete comment endpoint is not available
        axios.post(`${API_BASE_URL}/comments`, {
            userId: 1,
            postId: postId,
            name: "TEST COMMENT",
            body: commentBody,
            email: "nabila.lilipaly@gmail.com"
        })
        .then((res) => {
            const newListComment = commentList.filter((item) => item.body !== commentBody);

            //mock new comment list with fake data (deleted before)
            setCommentList(newListComment);
            setCommentText("");
        })
        .catch((e) => {
            console.log("ERROR SUBMIT COMMENT", e)
        })
    }

    function onClickEdit(commentBody) {
        setEditingComment(!isEditingComment);
        setEditCommentText(commentBody);
    }

    function onEditComment(commentBody) {
        const newListComment = commentList.map((item) => {
            if(item.body === commentBody) {
                return {
                    ...item,
                    body: editCommentText
                }
            } else return item
        });

        setEditingComment(false);
        setCommentList(newListComment);
    }

    return(
        <div>
            <Modal isOpen={isOpen} toggle={toggle} className="modal-comment">
                <div className="post-wrapper">
                    <div className="post-header">
                        <div className="post-user-avatar"/>
                        <span>username</span>
                    </div>
                    <span>{postItem?.title}</span>
                    <span>{postItem?.body}</span>
                </div>
                { isFetching && <Spinner color="secondary" children="" /> }
                {
                    commentList.map((comment, index) => {
                        return(
                            <div key={index} className="comment-items">
                                <div className="comment-item-header">
                                    <div className="comment-user-avatar"/>
                                    <span>{comment?.email}</span>
                                </div>
                                {
                                    isEditingComment && comment.email === "nabila.lilipaly@gmail.com"?
                                    <Form onSubmit={() => onEditComment(comment.body)}>
                                        <Input 
                                            type="text"
                                            name="editCommentText"
                                            value={editCommentText}
                                            onChange={(e) => setEditCommentText(e.target.value)}
                                            placeholder="Write a comment..."
                                        />
                                    </Form>
                                    :
                                    <div>
                                        <span>{comment?.body}</span>
                                        {
                                            comment.email === "nabila.lilipaly@gmail.com" &&
                                            <div className="edit-delete-wrapper">
                                                <span onClick={() => onClickEdit(comment.body)}>Edit</span>
                                                <span onClick={() => onDeleteComment(comment.body)}>Delete</span>
                                            </div>
                                        }
                                    </div>
                                }
                            </div>
                        )
                    })
                }
                <div className="enter-comment-wrapper">
                    <div className="comment-user-avatar"/>
                    <Form onSubmit={() => onSubmitComment}>
                        <Input 
                            type="textarea"
                            name="commentText"
                            value={commentText}
                            onChange={(e) => setCommentText(e.target.value)}
                            placeholder="Write a comment..."
                        />
                    </Form>
                </div>
                <div className="button-wrapper">
                    <Button onClick={onSubmitComment}>Post</Button>
                </div>
            </Modal>
        </div>
    )
}