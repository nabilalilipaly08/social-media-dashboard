import React, { useState, useEffect } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";

import { API_BASE_URL } from "../constants";

import MyTimeline from "../components/MyTimeline";
import AlbumGrid from "../components/AlbumGrid";
import Comments from "../PageHome/comments";

function PageUser({ location }) {
    const [userPostList, setUserPostList] = useState([]);
    const [userAlbumList, setUserAlbumList] = useState([]);
    const [listUsers, setListUsers] = useState([]);
    const [isOpenComment, setOpenModal] = useState(false);
    const [postId, setPostId] = useState(1);
    const [postItem, setPostItem] = useState(null);

    useEffect(() => {
        axios.get(`${API_BASE_URL}/users`)
        .then((res) => {
            setListUsers(res.data)
        })
        .catch((e) => console.log("ERRORNYA APA", e))

        axios.get(`${API_BASE_URL}/posts?userId=${location?.state?.userId}`)
        .then((res) => setUserPostList(res.data))
    }, [])

    useEffect(() => {
        axios.get(`${API_BASE_URL}/albums?userId=${location?.state?.userId}`)
        .then((res) => setUserAlbumList(res.data))
    }, [])

    function onToggleModal(modalPostId) {
        const modalPostItem = userPostList.find((item) => item.id === modalPostId);

        setPostItem(modalPostItem)
        setOpenModal(!isOpenComment)
        setPostId(modalPostId)
    }

    return(
        <Container className="homepage-container">
            { 
                isOpenComment && <Comments postId={postId} isOpen={isOpenComment} toggle={() => onToggleModal(postId)} postItem={postItem} /> 
            }
            <Row>
                <Col md={4} xs={12}>
                    <AlbumGrid userAlbumList={userAlbumList} />
                </Col>
                <Col md={8} xs={12}>
                    <MyTimeline 
                        listPosts={userPostList}
                        listUsers={listUsers}
                        onToggleModal={onToggleModal}
                    />
                </Col>
            </Row>
        </Container>
    )
}

export default withRouter(PageUser);