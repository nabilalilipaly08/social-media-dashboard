import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

//components
import PageHome from "./PageHome";
import PageUser from "./PageUser";
import Header from "./components/Header";

export default function AppRoutes() {
    return(
        <Router>
            <Header/>
            <Switch>
                <Route exact path="/">
                    <PageHome />
                </Route>
                <Route exact path="/:username">
                    <PageUser />
                </Route>
            </Switch>
        </Router>
    )
}