import React, { useState } from "react";
import { Form, Input, Button } from "reactstrap";
import UserAvatar from "../assets/nabila.jpg";

export default function MyTimeline({ listPosts, listUsers, onToggleModal, onSubmitPost, isHomepage }) {
    const [postText, setPostText] = useState("");

    return(
        <div className="timeline-wrapper">
            {
                isHomepage && (
                    <>
                        <p>HOME</p>
                        <div className="user-post">
                            <img src={UserAvatar} />
                            <Form>
                                <Input 
                                    type="textarea"
                                    value={postText}
                                    onChange={(e) => setPostText(e.target.value)}
                                    placeholder="What's happening?" /> 
                            </Form>
                        </div>
                        <div className="button-wrapper">
                            <Button onClick={() => onSubmitPost(postText)}>Post</Button>
                        </div>
                    </>
                )
            }
            {
                listPosts.map((posts, index) => {
                    const dataUser = listUsers.find((users) => users.id === posts.userId);

                    return(
                        <div key={index} className="timeline-posts-wrapper">
                            <div className="timeline-user-avatar"/>
                            <div className="timeline-post-text">
                                <div className="username-wrapper">
                                    <span>{dataUser?.name}</span>
                                    <span>@{dataUser?.username}</span>
                                </div>
                                <div className="post-wrapper">
                                    <span>{posts?.title}</span>
                                    <span>{posts?.body}</span>
                                </div>
                                <div onClick={() => onToggleModal(posts.id)} className="comment-button">Comments</div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}