import React, { useState, useEffect } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Container } from "reactstrap";

import { API_BASE_URL } from "../constants";

export default function PhotoItemCard({ dataAlbumItem, toggleModalAlbum }) {
    const [userPhotoList, setUserPhotoList] = useState([]);

    useEffect(() => {
        axios.get(`${API_BASE_URL}/photos?albumId=${dataAlbumItem.id}`)
        .then((res) => {
            setUserPhotoList(res.data)
        })
    }, [dataAlbumItem])

    const albumThumbnail = userPhotoList.filter((item) => item.albumId === dataAlbumItem.id);

    return(
        <div className="album-card-wrapper" onClick={toggleModalAlbum}>
            <div className="album-card-item">
                <img src={albumThumbnail?.[0]?.thumbnailUrl?? null}/>
            </div>
            <span>{dataAlbumItem?.title}</span>
        </div>
    )
}