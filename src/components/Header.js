import React from "react";
import { useHistory } from "react-router-dom";

import MyPhoto from "../assets/nabila.jpg";

export default function Header() {
    let history = useHistory();

    function goToHome() {
        history.push("/");
    }

    return(
        <div className="header-wrapper">
                <span onClick={goToHome}>MY SOCIAL NETWORK</span>
                <div>
                    <img src={MyPhoto} />
                    <span>Nabila K. Lilipaly</span>
                </div>
        </div>
    )
}