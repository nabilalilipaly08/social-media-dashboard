import React, { useState, useEffect } from "react";
import axios from "axios";
import { Modal, Spinner } from "reactstrap";

import { API_BASE_URL } from "../constants";

const ModalPhotoDetail = ({ isOpen, onToggle, imgUrl, title }) => (
    <Modal isOpen={isOpen} toggle={onToggle} className="modal-details">
        <div>
            <img src={imgUrl} />
        </div>
        <span>{title}</span>
    </Modal>
)

export default function ModalPhotos({ onToggle, isOpen, albumId }) {
    const [userPhotoList, setUserPhotoList] = useState([]);
    const [isOpenDetail, setOpenDetail] = useState(false);
    const [photoURL, setPhotoURL] = useState(null);
    const [titlePhoto, setTitlePhoto] = useState("");

    useEffect(() => {
        axios.get(`${API_BASE_URL}/photos?albumId=${albumId}`)
        .then((res) => {
            setUserPhotoList(res.data)
        })
    }, [isOpen])

    function onTogglePhoto(imgURL = null, title = "") {
        setOpenDetail(!isOpenDetail)
        setPhotoURL(imgURL)
        setTitlePhoto(title)
    }

    return(
        <Modal isOpen={isOpen} toggle={onToggle} className="modal-photos">
            <ModalPhotoDetail isOpen={isOpenDetail} onToggle={onTogglePhoto} imgUrl={photoURL} title={titlePhoto} />
            <div className="grid-album-wrapper">
            {
                userPhotoList.map((item, index) => 
                    <div className="album-card-wrapper" key={index} onClick={() => onTogglePhoto(item.url, item.title)}>
                        <div className="album-card-item">
                            <img src={item?.thumbnailUrl} />
                        </div>
                        <span>{item?.title}</span>
                    </div>
                )
            }
            </div>
        </Modal>
    )
}