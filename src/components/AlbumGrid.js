import React, { useState, useEffect } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Container } from "reactstrap";

import PhotoItemCard from "./PhotoItemCard";
import ModalPhotos from "./ModalPhotos";

export default function AlbumGrid({ userAlbumList }) {
    const [isOpenModal, setOpenModal] = useState(false);
    const [albumId, setAlbumId] = useState(null);

    function toggleModalAlbum(id) {
        setOpenModal(!isOpenModal)
        setAlbumId(id)
    }

    return(
        <div className="grid-album-wrapper">
        {
            isOpenModal && <ModalPhotos isOpen={isOpenModal} onToggle={toggleModalAlbum} albumId={albumId} />
        }
            {
                userAlbumList.map((item, index) => 
                    <PhotoItemCard dataAlbumItem={item} key={index} toggleModalAlbum={() => toggleModalAlbum(item.id)}/>
                )
            }
        </div>
    )
}